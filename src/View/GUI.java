package View;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import Control.Controller;

public class GUI {
	private JFrame frame;
	private JPanel panelCombobox;
	private JPanel panelInput;
	private JPanel panelOutput;
	private JTextArea textInput;
	private JTextArea textOutput;
	private JButton runButton;
	private JComboBox<String> formList;
	private Controller control;
	private String str;
	private JLabel input;
	private JLabel output;
	
	class ListenerMgr implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			 if (formList.getSelectedItem() == "one") {
				setResult(control.getFormOne(textInput.getText()));
			} 
			else if (formList.getSelectedItem() == "two") {
				setResult(control.getFormTwo(textInput.getText()));
			} 
			else if (formList.getSelectedItem() == "three"){
				setResult(control.getFormThree(textInput.getText()));
			}
			else if (formList.getSelectedItem() == "four"){
				setResult(control.getFormFour(textInput.getText()));
			}
		}
	}
	
	public GUI(){
		control = new Controller();
		createFrame();
	}
	public void createFrame(){
		frame = new JFrame();
		frame.setSize(690, 300);
		frame.setLayout(null);
		
		//input box		
		panelInput = new JPanel();
		panelInput.setLayout(null);
		panelInput.setBounds(222, 10, 202, 200);
		frame.add(panelInput);
		textInput = new JTextArea();
		textInput.setBorder(BorderFactory.createLineBorder(Color.black));
		textInput.setBounds(10, 10, 183, 170);
		panelInput.add(textInput);
		
		//output box
		panelOutput = new JPanel();
		panelOutput.setLayout(null);
		panelOutput.setBounds(455, 10, 202, 200);
		frame.add(panelOutput);
		textOutput = new JTextArea();
		textOutput.setBorder(BorderFactory.createLineBorder(Color.black));
		textOutput.setBounds(10, 10, 183, 170);
		panelOutput.add(textOutput);
		
		//run button
		runButton = new JButton("run");
		runButton.setBounds(275, 215, 100, 25);
		frame.add(runButton);
		
		runButton.addActionListener(new ListenerMgr());
		
		panelCombobox = new JPanel();
		panelCombobox.setLayout(null);
		panelCombobox.setBounds(10, 10, 300, 200);
		frame.add(panelCombobox);
		formList = new JComboBox<String>();
		panelCombobox.add(formList);
		formList.addItem("one");
		formList.addItem("two");
		formList.addItem("three");
		formList.addItem("four");
		
		
		formList.setBounds(10, 10, 180, 30);
		
		frame.setVisible(true);
	}
	public void setResult(String str){
		this.str = str;
		textOutput.setText(str);
	}
	
	public void extendResult(String str) {
		this.str = this.str+"\n"+str;
		textOutput.setText(this.str);
	}
}


