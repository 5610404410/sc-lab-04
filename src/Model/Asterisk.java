package Model;

import java.util.ArrayList;

public class Asterisk {
	ArrayList<String> setOne;
	ArrayList<String> setTwo;
	ArrayList<String> setThree;
	ArrayList<String> setFour;
	
	public Asterisk(){
		setOne = new ArrayList<String>();
		setTwo = new ArrayList<String>();
		setThree = new ArrayList<String>();
		setFour = new ArrayList<String>();
	}
	public ArrayList<String> formOne(String number){
		int a = Integer.parseInt(number);
		for (int i=0; i<a; i++){
			for (int j=0; j<a; j++){
				setOne.add("*");
			}
			setOne.add("\n");
		}
		return setOne;
	}
	public ArrayList<String> formTwo(String number){
		int a = Integer.parseInt(number);
		for (int i=0; i<a; i++){
			for (int j=0; j<=i; j++){
				setTwo.add("*");
			}
			setTwo.add("\n");
		}
		return setTwo;
	}
	public ArrayList<String> formThree(String number){
		int a = Integer.parseInt(number);
		for (int i=1; i<=a; i++){
			for (int j=1; j<=(a+2); j++){
				if ((j%2) == 0){
					setThree.add("*");
				}
				else{
					setThree.add("-");
				}				
			}
			setThree.add("\n");
		}
		return setThree;
	}
	public ArrayList<String> formFour(String number){
		int a = Integer.parseInt(number);
		for (int i=1; i<=a; i++){
			for (int j=1; j<=(a+2); j++){
				if ((i+j) % 2 == 0){
					setFour.add("*");
				}
				else{
					setFour.add(" ");
				}
			}
			setFour.add("\n");
		}
		return setFour;
	}
}
