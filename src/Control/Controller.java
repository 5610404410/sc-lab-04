package Control;

import java.util.ArrayList;

import Model.Asterisk;

public class Controller {	
	private Asterisk asterisk;
	
	public Controller(){
		asterisk = new Asterisk();
	}
	public String getFormOne(String number){
		String text1 = "";
		ArrayList<String> x = asterisk.formOne(number);
		for (int i=0; i<x.size(); i++){
			text1 += x.get(i);
		}
		return text1;
	}
	public String getFormTwo(String number){
		String text2 = "";
		ArrayList<String> y = asterisk.formTwo(number);
		for (int i=0; i<y.size(); i++){
			text2 += y.get(i);
		}
		return text2;
	}
	public String getFormThree(String number){
		String text3 = "";
		ArrayList<String> z = asterisk.formThree(number);
		for (int i=0; i<z.size(); i++){
			text3 += z.get(i);
		}
		return text3;
	}
	public String getFormFour(String number){
		String text4 = "";
		ArrayList<String> w = asterisk.formFour(number);
		for (int i=0; i<w.size(); i++){
			text4 += w.get(i);
		}
		return text4;
	}
}
